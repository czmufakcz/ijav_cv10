# Zadání úkolů

1. Převezměte z jiných projektů třídu User a enum ROLE.
    + Vytvořte kolekci od 3 uživatelích.
    + Proveďte serializaci do XML.
    + Proveďte deserializaci do XML.
    + Vypište kolekci.
2. Aplikujte bod 1 pro JSON za pomocí externí knihovny Jackson.
3. Rozšiřte náš chat o možnost ukládání. (Brainstorming)
