

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName(value = "user")
public class User implements Serializable {
    @JsonIgnore
    private String username;
    private String password;
    private int age;
    private ROLE role;
    private LocalDateTime created;

    @Override
    public String toString() {
        return "User [username=" + username + ", password=" + password + ", age=" + age + ", role=" + role + ", created=" + created + "]";
    }
    
    public User() {
        
    }
    
    public User(String username, String password, int age, ROLE role) {
        super();
        this.username = username;
        this.password = password;
        this.age = age;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ROLE getRole() {
        return role;
    }

    public void setRole(ROLE role) {
        this.role = role;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

}
