import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {

    private static final String XML_FILE_NAME = "test.xml";
    private static final String JSON_FILE_NAME = "test.json";

    public static void main(String[] args) {

        List<User> users = new LinkedList<>();
        users.add(new User("Karel", "password", 55, ROLE.USER));
        users.add(new User("Simek", "password", 33, ROLE.USER));
        users.add(new User("Pepa", "password", 21, ROLE.ADMIN));

    

    }
    public void JSON(List<User> users) {
        try {
            // Serializace
            ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter()
                  .writeValue(new File(JSON_FILE_NAME), users);

            // Deserializace
            List<User> usersDes = mapper.readValue(new File(JSON_FILE_NAME), LinkedList.class);
            System.out.println(usersDes);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void XML(List<User> users) {
        try {
            // Serializace
            FileOutputStream fos = new FileOutputStream(XML_FILE_NAME);
            XMLEncoder encoder = new XMLEncoder(fos);

            encoder.writeObject(users);
            encoder.close();

            // Deserializace
            FileInputStream fis = new FileInputStream(XML_FILE_NAME);
            XMLDecoder decoder = new XMLDecoder(fis);

            List<User> usersDes = (LinkedList<User>) decoder.readObject();
            decoder.close();

            System.out.println(usersDes);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
