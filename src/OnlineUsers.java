import java.util.LinkedList;
import java.util.List;

public class OnlineUsers {
    private List<User> users;
    
    public OnlineUsers(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
    
    
}
